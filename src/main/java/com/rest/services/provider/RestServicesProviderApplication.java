package com.rest.services.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class RestServicesProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestServicesProviderApplication.class, args);
    }

    @RestController
    @RequestMapping("/hello")
    class RestProvider {

        @GetMapping
        public ResponseEntity hello(@RequestParam String name) {
            return ResponseEntity.ok(String.format("Hello %s", name));
        }
    }
}
